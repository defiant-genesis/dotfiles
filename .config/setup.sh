echo "[multilib]" > /etc/pacman.conf
echo "Include = /etc/pacman.d/mirrorlist" > /etc/pacman.conf

#install base programs
pacman -Sy
pacman -S i3 light-dm termite polybar base-devel discord steam emacs NetworkManager pulseaudio pavucontrol alsa-utils nm-connection-editor git light-locker xorg-server mesa-utils vulkan-tools xorg-apps xf86-video-amdgpu mesa lib32-mesa xf86-video-ati xf86-video-intel nvidia nvidia-utils lib32-nvidia-utils linux-lts linux-lts-headers linux-zen linuz-zen-headers nvidia-dkms

#install yay
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

#install yay programs
yay -S brave-bin

systemctl enable light-dm
systemctl enable NetworkManager
